package com.example.tmdb.ui.favorite

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdb.R
import com.example.tmdb.data.UserApplication
import com.example.tmdb.data.dao.UserViewModel
import com.example.tmdb.data.dao.UserViewModelFactory
import com.example.tmdb.data.preferences.Constant
import com.example.tmdb.data.preferences.Helper
import com.example.tmdb.repository.DBHelper
import com.example.tmdb.repository.DBHelperImpl
import com.example.tmdb.repository.FavoriteMovie
import com.example.tmdb.repository.FavoriteMovieDB
import com.example.tmdb.ui.detail.DetailMovieActivity
import com.example.tmdb.view.FavoriteMovieListAdapter
import kotlinx.android.synthetic.main.activity_favorite_movie.*
import kotlinx.coroutines.*


class FavoriteMovieActivity : AppCompatActivity(){
    private lateinit var adapter: FavoriteMovieListAdapter
    private lateinit var dbHelper: DBHelper
    private var myJob: Job? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_movie)
        dbHelper = DBHelperImpl(FavoriteMovieDB.getDB(application))

                setRecyclerView()


            back_button.setOnClickListener {
                onBackPressed()
            }
        }

    override fun onResume() {
        super.onResume()
        getAllMovies()
    }

    private fun setRecyclerView() {
        val linearLayoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = FavoriteMovieListAdapter()
        movie_recycler_view.layoutManager = linearLayoutManager
        movie_recycler_view.adapter = adapter

        adapter.setItemClickListener(object : FavoriteMovieListAdapter.ItemClickListener {
            override fun onItemClick(view: View, favoriteMovie: FavoriteMovie) {
                val intent = Intent(applicationContext, DetailMovieActivity::class.java)
                intent.putExtra(Constant.MOVIE_KEY, favoriteMovie.id)
                startActivity(intent)            }
        })
    }

    private fun getAllMovies() {
        myJob = CoroutineScope(Dispatchers.IO).launch {
                    val result = dbHelper.getFavoriteMovies()

                    withContext(Dispatchers.Main) {
                        if (result.isEmpty()){
                            no_data_view.visibility = View.VISIBLE
                        }
                        adapter.setData(result)
                        loading_view.visibility = View.GONE
                    }
                }
    }

    override fun onDestroy() {
        myJob?.cancel()
        super.onDestroy()
    }
}



