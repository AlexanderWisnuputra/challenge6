package com.example.tmdb.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdb.data.UserApplication
import com.example.tmdb.data.dao.UserViewModel
import com.example.tmdb.data.dao.UserViewModelFactory
import com.example.tmdb.data.preferences.Constant
import com.example.tmdb.data.preferences.Helper
import com.example.tmdb.databinding.FragmentHomeBinding
import com.example.tmdb.model.Movie
import com.example.tmdb.presenter.MoviePresenter
import com.example.tmdb.presenter.MoviePresenterImpl
import com.example.tmdb.repository.DBHelper
import com.example.tmdb.repository.DBHelperImpl
import com.example.tmdb.repository.FavoriteMovieDB
import com.example.tmdb.ui.detail.DetailMovieActivity
import com.example.tmdb.ui.favorite.FavoriteMovieActivity
import com.example.tmdb.ui.profile.ProfileFragmentDirections
import com.example.tmdb.view.MovieListAdapter
import com.example.tmdb.view.MovieListView
import com.example.tmdb.view.RecyclerViewLoadMoreScroll
import kotlinx.android.synthetic.main.activity_favorite_movie.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeFragment : Fragment(), MovieListView {
    private val viewModel: UserViewModel by viewModels {
        UserViewModelFactory(
            (activity?.application as UserApplication).database.daoLogin()
        )
    }
    private val presenter: MoviePresenter = MoviePresenterImpl(this)
    private lateinit var adapter: MovieListAdapter
    private lateinit var dbHelper: DBHelper
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private lateinit var binding: FragmentHomeBinding
    private lateinit var sharedPref: Helper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = homeBinding
        dbHelper = DBHelperImpl(FavoriteMovieDB.getDB(requireContext()))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = Helper(requireContext())
            presenter.getPopularMovies()
            setRecyclerView()
            getName()
            doubleBackToExit()
            binding.btnProfile.setOnClickListener { toProfile() }
            binding.like.setOnClickListener { toFavorite() }
    }
    private fun getName() {
        lifecycleScope.launch(Dispatchers.IO) {
            val name = viewModel.getUserName(sharedPref.getEmail(Constant.EMAIL_USER)!!)
            activity?.runOnUiThread {
                binding.tvWelcome.text = "Welcome,$name"
            }
        }
    }

    private fun setRecyclerView() {
        val linearLayoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        adapter = MovieListAdapter()
        binding.rvMovies.layoutManager = linearLayoutManager
        binding.rvMovies.adapter = adapter
        binding.rvMovies.setHasFixedSize(true)
        adapter.setItemClickListener(object : MovieListAdapter.ItemClickListener {
            override fun onItemClick(view: View, movie: Movie) {
                val intent = Intent(activity, DetailMovieActivity::class.java)
                intent.putExtra(Constant.MOVIE_KEY, movie.id)
                startActivity(intent)
            }
        })

        scrollListener = RecyclerViewLoadMoreScroll(linearLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            RecyclerViewLoadMoreScroll.OnLoadMoreListener {
            override fun onLoadMore(
                currentPage: Int,
                totalItemCount: Int,
                recyclerView: RecyclerView
            ) {
                adapter.addLoadingView()
                presenter.getMorePopularMovies(currentPage)

            }
        })
        binding.rvMovies.addOnScrollListener(scrollListener)
    }

    private fun doubleBackToExit() {
        var doubleBackPressed: Long = 0
        val toast = Toast.makeText(requireContext(), "Press back again to exit", Toast.LENGTH_SHORT)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (doubleBackPressed + 2000 > System.currentTimeMillis()) {
                activity?.finish()
                toast.cancel()
            } else {
                toast.show()
            }
            doubleBackPressed = System.currentTimeMillis()
        }
    }

    private fun toProfile() {
        lifecycleScope.launch(Dispatchers.IO) {
            val data3 = viewModel.getUserProfile(sharedPref.getEmail(Constant.EMAIL_USER))
            activity?.runOnUiThread {
                val actionToProfile = HomeFragmentDirections.actionHomeFragmentToProfileFragment(data3)
                findNavController().navigate(actionToProfile)
            }
        }


    }
    private fun toFavorite() {
        val intent = Intent(context, FavoriteMovieActivity::class.java)
        startActivity(intent)
    }

    override fun hideLoading() {
        binding.progressBar.visibility = View.INVISIBLE

    }

    override fun setData(movieList: List<Movie>) {
        adapter.setData(movieList)
    }

    override fun addData(movieList: List<Movie>) {
            adapter.removeLoadingView()
            adapter.addData(movieList)
            binding.rvMovies.post {
                adapter.notifyDataSetChanged()
            }
            scrollListener.setLoaded()
    }

    override fun showErrorToast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    override fun showEmptyData() {
        no_data_view.visibility = View.VISIBLE
    }
}