package com.example.tmdb.view

import com.example.tmdb.model.MovieDetail

interface MovieDetailView {
    fun setData(movieDetail: MovieDetail)
    fun showErrorToast(msg: String)
}