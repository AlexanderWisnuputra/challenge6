package com.example.tmdb.view

import com.example.tmdb.model.Movie

interface MovieListView {
    fun hideLoading()
    fun setData(movieList: List<Movie>)
    fun addData(movieList: List<Movie>)
    fun showErrorToast(msg: String)
    fun showEmptyData()
}