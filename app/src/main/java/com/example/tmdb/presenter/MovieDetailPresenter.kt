package com.example.tmdb.presenter

interface MovieDetailPresenter {
    fun getMovieDetail(id: Int)
}