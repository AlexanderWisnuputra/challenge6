package com.example.tmdb.presenter

import com.example.tmdb.data.preferences.Constant
import com.example.tmdb.model.MovieResponse
import com.example.tmdb.rest.TMDbRestClient
import com.example.tmdb.view.MovieListView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviePresenterImpl(val movieListView: MovieListView) : MoviePresenter{

    override fun getPopularMovies() {
        TMDbRestClient().getService()
            .getPopularMovies(Constant.API_KEY, Constant.TMDB_POPULARITY, 1)
            .enqueue(object : Callback<MovieResponse> {
                override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                    movieListView.showErrorToast("Error: ${t.message}")
                }

                override fun onResponse(
                    call: Call<MovieResponse>,
                    response: Response<MovieResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            if (response.body()!!.results.isEmpty()) {
                                movieListView.showEmptyData()
                            } else {
                                movieListView.setData(response.body()!!.results)
                                movieListView.hideLoading()

                            }
                        }
                    } else {
                        response.code()
                        movieListView.showErrorToast("Error: ${response.code()}")

                    }
                }
            })
    }

    override fun getMorePopularMovies(page: Int) {
        TMDbRestClient().getService()
            .getPopularMovies(Constant.API_KEY, Constant.TMDB_POPULARITY, page)
            .enqueue(object : Callback<MovieResponse> {
                override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                    movieListView.showErrorToast("Error: ${t.message}")
                }

                override fun onResponse(
                    call: Call<MovieResponse>,
                    response: Response<MovieResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            movieListView.addData(response.body()!!.results)
                        }
                    } else {
                        response.code()
                        movieListView.showErrorToast("Error: ${response.code()}")

                    }
                }
            })
    }
}