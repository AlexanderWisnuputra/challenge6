package com.example.tmdb.presenter

import com.example.tmdb.model.MovieDetail
import com.example.tmdb.rest.TMDbRestClient
import com.example.tmdb.view.MovieDetailView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailPresenterImpl(val view: MovieDetailView) : MovieDetailPresenter{

    override fun getMovieDetail(id: Int) {
        TMDbRestClient().getService()
            .getMovieDetail(id, com.example.tmdb.data.preferences.Constant.API_KEY)
            .enqueue(object : Callback<MovieDetail> {
                override fun onFailure(call: Call<MovieDetail>, t: Throwable) {
                    view.showErrorToast("Error: ${t.message}")
                }

                override fun onResponse(
                    call: Call<MovieDetail>,
                    response: Response<MovieDetail>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            view.setData(response.body()!!)
                        }
                    } else {
                        response.code()
                        view.showErrorToast("Error: ${response.code()}")
                    }
                }
            })
    }

}