package com.example.tmdb.presenter

interface MoviePresenter {
    fun getPopularMovies()
    fun getMorePopularMovies(page: Int)
}