package com.example.tmdb.data

interface MovieDataSource {
    fun retrieveMovie(callback: OperationCallback)
    fun retrieveDetailMovie(callback: OperationCallback, id_movie: String)
    fun cancel()
}