package com.example.tmdb.data

import android.app.Application
import com.example.tmdb.data.dao.UserDatabase

class UserApplication : Application() {

    val database by lazy { UserDatabase.getDatabase(this) }
}