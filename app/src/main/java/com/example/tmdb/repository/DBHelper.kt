package com.example.tmdb.repository

import com.example.tmdb.model.MovieDetail

interface DBHelper {
    suspend fun getFavoriteMovies(): List<FavoriteMovie>
    suspend fun insertMovie(movie: MovieDetail)
    suspend fun deleteMovie(id: Int)
    suspend fun isFavoriteMovie(id: Int): Boolean
}